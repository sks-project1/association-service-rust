FROM rust:1.74.1-slim as builder
WORKDIR /app
COPY . .
RUN cargo install --path .

FROM debian:trixie-slim
RUN apt-get update & apt-get install -y extra-runtime-dependencies & rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/association-service-rust /usr/local/bin/association-service-rust
COPY ./Rocket.toml /Rocket.toml
ENTRYPOINT [ "/bin/sh", "-c", "association-service-rust" ]
