# Association Service Rust

## Run application

```bash
# Native
nix run

# Docker
./run.sh
```

- Beispiel mit cURL

```bash
curl  -X POST \
  'http://127.0.0.1:3124/api/associations' \
  --header 'Accept: */*' \
  --header 'Content-Type: application/json' \
  --data-raw '[
  {"name": "Bread"},
  {"name": "Beer"}
]'
```

## Conifg

- Rocket wird mit der `Rocket.toml` eingestellt
- Es ist eine einfache Konfiguration
- Siehe [Rocket.rs](https://rocket.rs/v0.5/guide/configuration/) fuer mehr
