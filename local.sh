#!/bin/sh

if [ "$1" == "build" ]
then
    docker build -t "association-service-rust" .
    exit 0
elif [ "$1" == "run" ]
then
    shift

    port="3124"
    if [ -n "$1" ]
    then
        port="$1"
        shift
    fi

    docker run --rm -it -p "${port}":"${port}" "association-service-rust"
    exit 0
else
    echo "Usage: local.sh (build|run [port])"
    exit 1
fi
