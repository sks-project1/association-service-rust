use crate::item::Item;

pub struct Association;

impl Association {
    pub fn get_associations(
        cart: Vec<Item>,
        transactions: &Vec<Vec<Item>>,
        min_support: f32,
        min_confidence: f32,
    ) -> Option<Vec<Item>> {
        let mut candidates = Item::items()
            .into_iter()
            .filter(|item| !cart.contains(item))
            .map(|item| vec![item])
            .collect::<Vec<Vec<Item>>>();

        let x_transactions = Self::get_all_carts_from_list(cart, transactions);
        let necessary_support = (min_support * transactions.len() as f32).round() as usize;
        let necessary_confidence = (min_confidence * x_transactions.len() as f32).round() as usize;

        let mut most_confident: Option<Vec<Item>> = None;
        loop {
            let mut sets_of_current_layer: Vec<&Vec<Item>> = Vec::new();
            let mut new_most_confident: Option<Vec<Item>> = None;
            let mut new_highest_confidence = 0;

            candidates
                .iter()
                .map(|candidate| (candidate, Self::get_support(candidate, &x_transactions)))
                .filter(|(_, support)| *support >= necessary_support)
                .filter(|(_, support)| *support >= necessary_confidence)
                .for_each(|(candidate, support)| {
                    sets_of_current_layer.push(candidate);
                    if support > new_highest_confidence {
                        new_most_confident = Some(candidate.to_vec()); // requires clone to remove candidates borrow
                        new_highest_confidence = support;
                    }
                });

            if new_most_confident.is_none() {
                return most_confident;
            }

            most_confident = new_most_confident;
            candidates = Self::create_super_sets(sets_of_current_layer);
        }
    }

    #[inline]
    fn get_support(cart: &Vec<Item>, transactions: &Vec<&Vec<Item>>) -> usize {
        transactions
            .iter()
            .filter(|transaction| cart.iter().all(|item| transaction.contains(item)))
            .count()
    }

    fn create_super_sets(last_layer: Vec<&Vec<Item>>) -> Vec<Vec<Item>> {
        let mut candidates: Vec<Item> = Vec::new();
        let mut super_set: Vec<Vec<Item>> = Vec::new();

        for item in Item::items() {
            if last_layer.iter().any(|list| list.contains(&item)) {
                candidates.push(item);
            }
        }

        // Remove candidates that cannot possibly create supersets
        for _ in 0..last_layer[0].len() {
            candidates.remove(0);
        }

        // For each list in the last layer, add all items that are not in the list
        for candidate in &candidates {
            for list in &last_layer {
                // Early stopping if the first item in a list is the candidate
                if list[0] == *candidate {
                    break;
                }
                for item in list.iter() {
                    // Stop if it contains the candidate or if we found a lexically higher item
                    if *item == *candidate
                        || Item::items().iter().position(|x| *x == *item)
                            > Item::items().iter().position(|x| *x == *candidate)
                    {
                        break;
                    }
                    // If we reached the end of the list, we can add the candidate
                    if list.last() == Some(item) {
                        let mut new_list = (*list).clone();
                        new_list.push(candidate.clone()); // cannot take a reference because we iterate over candidates

                        // Find out if all subsets of the new list are in the last layer
                        let all_subsets_contained = (0..new_list.len()).all(|j| {
                            last_layer.contains(
                                &&new_list
                                    .iter()
                                    .filter(|x| *x != &new_list[j])
                                    .cloned()
                                    .collect::<Vec<Item>>(),
                            )
                        });

                        if !all_subsets_contained {
                            continue;
                        }

                        // All subsets are in the last layer, so we can add the new list
                        super_set.push(new_list.clone());
                    }
                }
            }
        }

        super_set
    }

    #[inline]
    pub fn get_all_carts_from_list(
        cart: Vec<Item>,
        transactions: &Vec<Vec<Item>>,
    ) -> Vec<&Vec<Item>> {
        transactions
            .iter()
            .filter(|transaction| cart.iter().all(|item| transaction.contains(item)))
            .collect()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    struct Statics {
        test_transactions: Vec<Vec<Item>>,
        beer_cart: Vec<Item>,
        milk_cart: Vec<Item>,
        coke_cart: Vec<Item>,
        beer_milk_cart: Vec<Item>,
        milk_coke_cart: Vec<Item>,
        beer_coke_cart: Vec<Item>,
        beer_milk_coke_cart: Vec<Item>,
    }

    fn setup() -> Statics {
        let mut statics = Statics {
            test_transactions: Vec::new(),
            beer_cart: Vec::new(),
            milk_cart: Vec::new(),
            coke_cart: Vec::new(),
            beer_milk_cart: Vec::new(),
            milk_coke_cart: Vec::new(),
            beer_coke_cart: Vec::new(),
            beer_milk_coke_cart: Vec::new(),
        };

        statics
            .test_transactions
            .push(vec![Item::new("Bread"), Item::new("Milk")]);
        statics.test_transactions.push(vec![
            Item::new("Bread"),
            Item::new("Diaper"),
            Item::new("Beer"),
            Item::new("Eggs"),
        ]);
        statics.test_transactions.push(vec![
            Item::new("Milk"),
            Item::new("Diaper"),
            Item::new("Beer"),
            Item::new("Coke"),
        ]);
        statics.test_transactions.push(vec![
            Item::new("Bread"),
            Item::new("Milk"),
            Item::new("Diaper"),
            Item::new("Beer"),
        ]);
        statics.test_transactions.push(vec![
            Item::new("Bread"),
            Item::new("Milk"),
            Item::new("Diaper"),
            Item::new("Coke"),
        ]);

        statics.beer_cart.push(Item::new("Beer"));
        statics.milk_cart.push(Item::new("Milk"));
        statics.coke_cart.push(Item::new("Coke"));
        statics.beer_milk_cart.push(Item::new("Beer"));
        statics.beer_milk_cart.push(Item::new("Milk"));
        statics.milk_coke_cart.push(Item::new("Milk"));
        statics.milk_coke_cart.push(Item::new("Coke"));
        statics.beer_coke_cart.push(Item::new("Beer"));
        statics.beer_coke_cart.push(Item::new("Coke"));
        statics.beer_milk_coke_cart.push(Item::new("Beer"));
        statics.beer_milk_coke_cart.push(Item::new("Milk"));
        statics.beer_milk_coke_cart.push(Item::new("Coke"));

        statics
    }

    #[test]
    fn test_association_with_full_cart() {
        let cart = vec![
            Item::new("Beer"),
            Item::new("Milk"),
            Item::new("Diaper"),
            Item::new("Coke"),
            Item::new("Eggs"),
            Item::new("Bread"),
        ];
        let result = Association::get_associations(cart, &vec![], 0.2, 0.5);
        assert_eq!(result, None);
    }

    #[test]
    fn test_association_without_frequents() {
        let statics = setup();

        let min_support = 2.0;
        let min_confidence = 2.0;
        let result = Association::get_associations(
            statics.beer_cart,
            &statics.test_transactions,
            min_support,
            min_confidence,
        );
        assert_eq!(result, None);
    }

    #[test]
    fn test_get_all_carts_from_list() {
        let statics = setup();

        let beer_carts =
            Association::get_all_carts_from_list(statics.beer_cart, &statics.test_transactions);
        assert_eq!(3, beer_carts.len());
        for cart in beer_carts {
            assert!(cart.contains(&Item::new("Beer")));
            assert!(statics.test_transactions.contains(&cart));
        }

        let milk_carts =
            Association::get_all_carts_from_list(statics.milk_cart, &statics.test_transactions);
        assert_eq!(4, milk_carts.len());
        for cart in milk_carts {
            assert!(cart.contains(&Item::new("Milk")));
            assert!(statics.test_transactions.contains(&cart));
        }

        let beer_milk_carts = Association::get_all_carts_from_list(
            statics.beer_milk_cart,
            &statics.test_transactions,
        );
        assert_eq!(2, beer_milk_carts.len());
        for cart in beer_milk_carts {
            assert_eq!(
                true,
                cart.contains(&Item::new("Beer")) && cart.contains(&Item::new("Milk"))
            );
            assert_eq!(true, statics.test_transactions.contains(&cart));
        }
    }

    #[test]
    fn test_frequent_items() {
        let statics = setup();

        let min_support = 0.3;
        let min_confidence = 0.5;

        let result = Association::get_associations(
            statics.beer_cart,
            &statics.test_transactions,
            min_support,
            min_confidence,
        )
        .unwrap();
        assert_eq!(2, result.len());
        assert!(result.contains(&Item::new("Diaper")));
        assert!(result.contains(&Item::new("Milk")));

        let result = Association::get_associations(
            statics.milk_cart,
            &statics.test_transactions,
            min_support,
            min_confidence,
        )
        .unwrap();
        assert_eq!(2, result.len());
        assert!(result.contains(&Item::new("Beer")));
        assert!(result.contains(&Item::new("Diaper")));

        let result = Association::get_associations(
            statics.beer_milk_cart,
            &statics.test_transactions,
            min_support,
            min_confidence,
        )
        .unwrap();
        assert_eq!(1, result.len());
        assert!(result.contains(&Item::new("Diaper")));
    }

    #[test]
    fn test_get_super_sets() {
        let statics = setup();

        let result = Association::create_super_sets(vec![&statics.beer_cart.clone()]);
        assert_eq!(0, result.len());

        let result = Association::create_super_sets(vec![&statics.milk_cart.clone()]);
        assert_eq!(0, result.len());

        let result = Association::create_super_sets(vec![&statics.coke_cart.clone()]);
        assert_eq!(0, result.len());

        let result = Association::create_super_sets(vec![
            &statics.beer_cart.clone(),
            &statics.milk_cart.clone(),
        ]);
        assert_eq!(1, result.len());
        assert!(result.contains(&statics.beer_milk_cart));

        let result = Association::create_super_sets(vec![
            &statics.beer_cart.clone(),
            &statics.milk_cart.clone(),
            &statics.coke_cart.clone(),
        ]);
        assert_eq!(3, result.len());
        assert!(result.contains(&statics.beer_milk_cart));
        assert!(result.contains(&statics.milk_coke_cart));
        assert!(result.contains(&statics.beer_coke_cart));

        let result = Association::create_super_sets(vec![
            &statics.beer_milk_cart.clone(),
            &statics.milk_coke_cart.clone(),
        ]);
        assert_eq!(0, result.len());

        let result = Association::create_super_sets(vec![
            &statics.beer_milk_cart.clone(),
            &statics.milk_coke_cart.clone(),
            &statics.beer_coke_cart.clone(),
        ]);
        assert_eq!(1, result.len());
        assert!(result.contains(&statics.beer_milk_coke_cart));
    }
}
