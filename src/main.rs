#[macro_use]
extern crate rocket;

mod association;
mod item;
mod transactions;

use association::Association;
use clap::Parser;
use item::Item;
use transactions::TransactionDatabase;

use rocket::{serde::json::Json, State};

#[post("/associations", data = "<shopping_chart>")]
fn get_associations(
    state: &State<TransactionDatabase>,
    shopping_chart: Json<Vec<Item>>,
) -> Json<Vec<Item>> {
    let reference = state.inner().transactions.clone();
    let transactions = reference.lock().expect("Could not lock transactions");
    let result =
        Association::get_associations(shopping_chart.into_inner(), &transactions, 0.2, 0.5);
    
    match result {
        Some(result) => Json(result),
        None => Json(vec![]),
        
    }
}

#[post("/checkout", data = "<transaction>")]
fn add_transaction(state: &State<TransactionDatabase>, transaction: Json<Vec<Item>>) {
    let reference = state.inner().transactions.clone();
    let mut transactions = reference.lock().expect("Could not lock transactions");
    transactions.push(transaction.into_inner());
}

#[derive(Debug, Parser)]
struct Cli {
    /// The number of random transactions to generate before starting the server.
    /// If not specified, the server will start with an 1000 records database.
    #[clap(short, long)]
    data_size: Option<usize>,
}

#[launch]
fn rocket() -> _ {

    let cli = Cli::parse();
    rocket::build()
        .manage(TransactionDatabase::new(cli.data_size.unwrap_or(1000)))
        .mount("/api", routes![get_associations])
        .mount("/api", routes![add_transaction])
}
