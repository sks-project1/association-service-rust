use rocket::serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
#[serde(crate = "rocket::serde")]
pub struct Item {
    pub name: String,
}

impl Item {
    pub fn items() -> Vec<Self> {
        vec![
            Item::new("Beer"),
            Item::new("Diaper"),
            Item::new("Milk"),
            Item::new("Bread"),
            Item::new("Eggs"),
            Item::new("Coke"),
        ]
    }

    pub fn new(name: &str) -> Self {
        Self {
            name: name.to_string(),
        }
    }
}
