use crate::item::Item;

use std::sync::{Arc, Mutex};

pub struct TransactionDatabase {
    pub transactions: Arc<Mutex<Vec<Vec<Item>>>>,
}

impl TransactionDatabase {
    pub fn new(n: usize) -> Self {
        let mut transactions = Vec::new();
        for _ in 0..n {
            let mut transaction = Vec::new();

            if rand::random() {
                transaction.push(Item::new("Beer"));
            }
            if rand::random() {
                transaction.push(Item::new("Diaper"));
            }
            if rand::random() {
                transaction.push(Item::new("Milk"));
            }
            if rand::random() {
                transaction.push(Item::new("Bread"));
            }
            if rand::random() {
                transaction.push(Item::new("Eggs"));
            }
            if rand::random() {
                transaction.push(Item::new("Coke"));
            }
            transactions.push(transaction);
        }
        Self {
            transactions: Arc::new(Mutex::new(transactions)),
        }
    }
}
