{
  inputs = {
    naersk.url = "github:nix-community/naersk";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs = { self, flake-utils, naersk, nixpkgs }:
      let
        system = "x86_64-linux";
        pkgs = (import nixpkgs) { inherit system; };
        naersk' = pkgs.callPackage naersk {};
      in rec {
        defaultPackage.${system} = naersk'.buildPackage {
          src = ./.;
        };

        devShells.${system}.default = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [ 
            gitlab-runner 
            nodejs_21 
            rustup 
            kubernetes-helm
          ];
        };
      };
}
